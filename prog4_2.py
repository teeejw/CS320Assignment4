import sys
exec(open('StackMachine.py').read())

print("Assignment #4-2, Thomas Webb, tjwebb@comast.net")

with open(sys.argv[1], 'r') as f:
	tokens = [line.split() for line in f.readlines()]
	sm = StackMachine()
	
	for i in range(0,len(tokens)):
		for j in range(0,len(tokens[i])):
			if len(tokens[i]) == 2:
				if tokens[i][j] == "push" and tokens[i][j+1].isdigit():
					if "." in tokens[i][j+1]:
						sm.push(float(tokens[i][j+1]))
					else: 
						sm.push(int(tokens[i][j+1]))
			if len(tokens[i]) == 1:
				if tokens[i][j] == "pop": print(StackMachine.pop(sm))
				if tokens[i][j] == "add": StackMachine.add(sm)
				if tokens[i][j] == "sub": StackMachine.sub(sm)
				if tokens[i][j] == "mul": StackMachine.mul(sm)
				if tokens[i][j] == "div": StackMachine.div(sm)
				if tokens[i][j] == "mod": StackMachine.mod(sm)
