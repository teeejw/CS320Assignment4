import sys

print("Assignment #4-1, Thomas Webb, tjwebb@comast.net")

with open(sys.argv[1], 'r') as f:
    lines = f.readlines()
    tokens = [l.split() for l in lines]
    for i in range(0,len(tokens)):
        for j in range(0,len(tokens[i])):
            print(tokens[i][j],end='')
            if j < len(tokens[i])-1:
                print (",",end='')
        print()
