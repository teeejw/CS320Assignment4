class StackMachine:
	def __init__(self):
		self._data = []

	def push(self,x):
		self._data.append(x)

	def pop(self):
		return self._data.pop() if len(self._data) > 0 else None

	def add(self):
		if len(self._data) >= 2:
			self.push(self._data.pop() + self._data.pop())

	def sub(self):
		if len(self._data) >= 2:
			self.push( -1 * (self._data.pop() - self._data.pop()))

	def mul(self):
		if len(self._data) >= 2:
			self.push(self._data.pop() * self._data.pop())

	def div(self):
		if len(self._data) >= 2:
			self.push( 1 / (self._data.pop() / self._data.pop()))

	def mod(self):
		if len(self._data) >= 2:
			self.push(self._data.pop() % self._data.pop())
