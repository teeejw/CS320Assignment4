prog4_1.py
This program uses sys.argv to get the name of the file to be tokenized from the command line. Then it tokenizes the input using the split() function deliminated by ' ' and prints each line with commas seperating the tokens.

StackMachine.py
This program defines the stack machine class. It has an iternal data structure to hold data. It has push, pop, add, sub, mul, div, and mod methods. 

prog4_2.py
This program tokenizes the input the same as prog4_2.py and then based on keywords and token length it calls the appropriate methods on the stack machine.
